FROM ruby:2.3

RUN apt-get update
RUN apt-get install -y git
RUN git clone https://shubham_jain_@bitbucket.org/shubham_jain_/tower_api.git
WORKDIR ./tower_api
RUN ls

RUN bundle install

ENV DATABASE_URL="mysql2://root:@192.168.1.10/currency"
EXPOSE 3000

RUN rake db:create
RUN rake db:migrate
RUN rake db:seed
RUN bundle exec crono start RAILS_ENV=development

RUN rails s -p 3000