# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'csv'

currencies = Currency.create([
							  {code: 'BTC', title: 'Bitcoin',price: 13610.6},
							  {code: 'DOGE', title: 'Dogecoin',price: 0.0126043},
							  {code: 'ETH', title: 'Ethereum',price: 1330.63},
							  {code: 'LTC', title: 'Litecoin',price: 242.514}
							 ])
# puts(currencies)
def import_prices(code, file)
	prices = []
	CSV.foreach(Rails.root.join('lib','CurrencyDumps',file), headers: true) do |row|
		row = row.to_h
		coin = {
			code: code,
			date: row['date'],
			tx_volume: row['txVolume(USD)'],
			tx_count: row['txCount'],
			price: row['price(USD)'],
			market_cap: row['marketcap(USD)'],
			exchange_volume: row['exchangeVolume(USD)'],
			generated_coins: row['generatedCoins'],
			fees: row['fees']
		}
		price = Price.new(coin)
		prices << price
	end
	Price.import(prices)
	# puts(prices[0].code,prices[0].fees)
end

import_prices('BTC','btc.csv')
import_prices('DOGE','doge.csv')
import_prices('ETH','eth.csv')
import_prices('LTC','ltc.csv')