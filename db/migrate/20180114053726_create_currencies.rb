class CreateCurrencies < ActiveRecord::Migration[5.0]
  def change
    create_table :currencies do |t|
      t.string :code
      t.string :title
      t.float :price
      t.float :volume
      t.float :market_cap
      t.timestamps
    end
  end
end
