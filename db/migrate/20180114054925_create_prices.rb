class CreatePrices < ActiveRecord::Migration[5.0]
  def change
    create_table :prices do |t|
      t.string :code	
      t.date :date
      t.float :tx_volume
      t.float :tx_count
      t.float :market_cap
      t.float :price 
      t.float :exchange_volume
      t.float :generated_coins
      t.float :fees
      t.timestamps
    end
  end
end
