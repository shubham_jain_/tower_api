class PriceSerializer < ActiveModel::Serializer
  attributes :id,:date,:tx_volume,:tx_count,:market_cap,:price,:exchange_volume,:generated_coins,:fees
end
