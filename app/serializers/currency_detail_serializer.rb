class CurrencyDetailSerializer < ActiveModel::Serializer
  attributes :code, :title, :price, :volume, :market_cap,:show_prices
  
  def show_prices
  	prices = []
  	object.prices.order(date: :desc).each do |price|
  		  	prices << PriceSerializer.new(price)
  	end
  	prices
  end
end
