Rails.application.routes.draw do
  resources :prices
  resources :currencies
  mount Crono::Web, at: '/crono'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
