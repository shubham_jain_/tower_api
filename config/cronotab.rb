# cronotab.rb — Crono configuration file
#
# Here you can specify periodic jobs and schedule.
# You can use ActiveJob's jobs from `app/jobs/`
# You can use any class. The only requirement is that
# class should have a method `perform` without arguments.
#
# class TestJob
#   def perform
#     puts 'Test!'
#   end
# end
#
# Crono.perform(TestJob).every 2.days, at: '15:30'
#
require 'rest-client'
require 'json'
require 'active_support'
class CurrentPrices

	@@url = "https://www.alphavantage.co/query"
	@@params = {
		function: "DIGITAL_CURRENCY_INTRADAY",
		datatype: "json",
		apikey: "S55G492JP4Z5P9E6",
		market: "USD"
	}
	def self.fetch()
		currencies = Currency.all
		currencies.each do |currency|
			@@params['symbol'] = currency.code
			response = JSON.parse(RestClient.get(@@url,{params: @@params}))
			# puts(response)
			response = response["Time Series (Digital Currency Intraday)"]
			date = response.keys[0]
			response = response[date]
			currency.price = response['1a. price (USD)']
			currency.volume = response['2. volume']
			currency.market_cap = response['3. market cap (USD)']
			puts(currency.price)
			currency.save			
		end
	end

end
class CurrentPricesJob
	def perform
		CurrentPrices.fetch()
	end
end

Crono.perform(CurrentPricesJob).every 2.minutes
